FIN=$1
FOUT=${FIN%.md}.tex

pandoc $FIN\
    --verbose\
    --to=latex\
    --no-highlight\
    --template=custom.latex\
    --metadata-file=metadata.yaml\
    -s --output=$FOUT;