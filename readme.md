# Markdown -> PDF : Un petit template rapide

Pandoc permet de créer un document pdf à partir d'un fichier markdown. Pour passer d'un fichier markdown à un pdf pandoc passe par une étape intermédiaire en latex. Pandoc utilise donc un template par défaut pour faire cette conversion.

Ce projet est une petite bidouille du template de base fournit par pandoc pour faire un document qui à l'air vaguement correcte.

Le template de base de pandoc peut être trouver à ici : 
https://raw.githubusercontent.com/jgm/pandoc-templates/master/default.latex

La documentation pandoc se trouve ici :
https://pandoc.org/MANUAL.html

La section de pandoc spécifique au markdown se trouve ici :
https://pandoc.org/MANUAL.html#pandocs-markdown

La section de pandoc sur les metadata se trouve ici :
https://pandoc.org/MANUAL.html#metadata-variables

La section de pandoc sur les metadata spécifique au latex se trouve ici :
https://pandoc.org/MANUAL.html#variables-for-latex

Le projet officiel LaTeX se trouve ici :
https://www.latex-project.org/help/

Une documentation non-officiel pas mal pour le latex :
https://www.overleaf.com/learn

## Utilisation

**!! Fonctionne probablement uniquement sous Linux (Xubuntu 18.04).**

Téléchargement du contenu du repo gitlab et extraction **(avec exemple)**:
```
wget https://gitlab.com/Karnaesca/markdown-to-pdf-quick-template/-/archive/master/markdown-to-pdf-quick-template-master.tar
tar -xf markdown-to-pdf-quick-template-master.tar
```

Téléchargement uniquement des fichiers necessaires **(sans exemple)**:
```
wget https://gitlab.com/Karnaesca/markdown-to-pdf-quick-template/-/raw/master/custom.latex
wget https://gitlab.com/Karnaesca/markdown-to-pdf-quick-template/-/raw/master/make.sh
wget https://gitlab.com/Karnaesca/markdown-to-pdf-quick-template/-/raw/master/metadata.yaml
```


Ajouter une entête à ton fichier markdown :
```
---
title: Titre du document
author: nom de l'auteur
date: la date
logo: le/chemin/logo.png
illu: le/chemin/illustration.png
...
```

Rendre le script executable:
```
chmod +x make.sh
```

Execution du script:
```
./make.sh ton_fichier_markdown.md
```

## A quoi servent les fichiers ?

+ `custom.latex` :  C'est le template custom, modifier à partir du template utilisé par pandoc
+ `illu.png` : placeholder pour une image d'illustration dans la partie titre du document
+ `logo.png` : placeholder pour un logo
+ `main.md` : exemple de fichier markdown
+ `main.pdf` : exemple de rendu pdf
+ `make.sh` : script qui construit le pdf, ça appelle juste pandoc et puis ouvre le pdf ensuite, c'est possible que l'ouverture de pdf ne fonctionne que sur xubuntu
+ `metadata.yaml` : une partie de l'entête qui est redondante à écrire à chaque début de fichier markdown. Ajoute un sommaire, la numerotation des sections, la couleurs des liens, la taille du document et des marges, la taille de la police.
+ `readme.md` : ce fichier

