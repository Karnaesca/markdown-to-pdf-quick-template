FIN=$1
FOUT=${FIN%.md}.pdf

pandoc $FIN\
    --verbose\
    --to=latex\
    --no-highlight\
    --template=custom.latex\
    --metadata-file=metadata.yaml\
    --output=$FOUT\
    --pdf-engine=xelatex;
xdg-open $FOUT;